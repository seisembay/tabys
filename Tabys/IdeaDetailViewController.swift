//
//  IdeaDetailViewController.swift
//  Tabys
//
//  Created by Islam Seisembay on 29/05/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

class IdeaDetailViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {
    
    var idea: Idea!
    var isNew: Bool = false
    
    var cellSize1: CGFloat {
        get {
            return view.frame.size.height / 12
        }
    }
    
    var addTasksTableViewHeightConstraint: NSLayoutConstraint?
    
    var scrollView: UIScrollView = {
        var scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()

    var contentView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        return view
    }()
    
    var titleTextField: UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
        textField.leftViewMode = .always
        var attributedPlaceholder = NSAttributedString(string: "Title", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12), NSAttributedString.Key.foregroundColor : UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)])
        textField.font = UIFont(name: SFFont.medium.title, size: 12)
        textField.attributedPlaceholder = attributedPlaceholder
        textField.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        textField.layer.cornerRadius = 6
        return textField
    }()
    
    var categoryTextField: UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
        textField.leftViewMode = .always
        var attributedPlaceholder = NSAttributedString(string: "Category", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12), NSAttributedString.Key.foregroundColor : UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)])
        textField.font = UIFont(name: SFFont.medium.title, size: 12)
        textField.attributedPlaceholder = attributedPlaceholder
        textField.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        textField.layer.cornerRadius = 6
        return textField
    }()
    
    var tasksLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tasks"
        label.font = UIFont(name: SFFont.medium.title, size: 14)
        label.textColor = UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)
        return label
    }()
    
    var tableView_1: UITableView = {
        var tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layer.cornerRadius = 8
        tableView.bounces = false
        tableView.isScrollEnabled = true
        return tableView
    }()
    
    var isOngoing_label: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedText = NSAttributedString(string: "Ongoing project", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 14), NSAttributedString.Key.foregroundColor : UIColor(red: 40/255, green: 40/255, blue: 40/255, alpha: 1)])
        label.attributedText = attributedText
        return label
    }()
    
    var isOngoing_switch: UISwitch = {
        var isOngoing_switch = UISwitch()
        isOngoing_switch.translatesAutoresizingMaskIntoConstraints = false
        isOngoing_switch.isOn = false
        isOngoing_switch.addTarget(self, action: #selector(isOngoingSwitchStateChanged(_:)), for: .valueChanged)
        return isOngoing_switch
    }()
    
    var stackView_0: UIStackView = {
        var stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if isEditing {
            print("you're still editing")
        }
    }
    
    override func viewDidLoad() {
        navigationItem.title = "Create Idea"
        navigationItem.rightBarButtonItem = editButtonItem
        
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        if idea == nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButtonTapped(sender:)))
            
            isNew = true
            idea = Idea(uuid: UUID().uuidString, title: "", category: Category(title: "", colour: .blue), tasks: [], isOngoing: false)
        } else {
            titleTextField.isUserInteractionEnabled = false
            categoryTextField.isUserInteractionEnabled = false
            isOngoing_switch.isEnabled = false
            tableView_1.isUserInteractionEnabled = false
            
            titleTextField.text = idea.title
            categoryTextField.text = idea.category.title
            isOngoing_switch.isOn = idea.isOngoing
            navigationItem.title = "Details"
            
            titleTextField.backgroundColor = .clear
            categoryTextField.backgroundColor = .clear
        }
        
        setupMain()
        setupTextFields()
        setupSwitch()
        setupTasks()
        
    }
    
    @IBAction func saveButtonTapped(sender: UIBarButtonItem) {
        print("save btn tapped")
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        navigationItem.title = editing ? "Edit Idea" : "Details"
        titleTextField.isUserInteractionEnabled = editing
        categoryTextField.isUserInteractionEnabled = editing
        isOngoing_switch.isEnabled = editing
        tableView_1.isUserInteractionEnabled = editing
        //tableView_1.setEditing(editing, animated: true)
        
        UIView.animate(withDuration: 0.5) {
            self.titleTextField.backgroundColor = editing ? UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1) : .clear
            self.categoryTextField.backgroundColor = editing ? UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1) : .clear
            
        }
        
        if !editing {
            NotificationCenter.default.post(name: ViewController.ideaNotification, object: nil, userInfo: ["Idea" : idea, "isNew" : isNew])
        }
        
    }
    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
//        switch editingStyle {
//        case .delete:
//            print("delete")
//        case .insert:
//            print("insert")
//        case .none:
//            print("none")
//        default:
//            print("no case")
//        }
//    }
    
    func setupMain() {
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        //view.translatesAutoresizingMaskIntoConstraints = false

        scrollView.delegate = self
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0),
            scrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
        scrollView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)

        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 0.0),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0.0),
            contentView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: 0.0),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0.0),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        var heightContraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        heightContraint.isActive = true
        heightContraint.priority = .defaultLow
    }
    
    func setupTextFields() {
        titleTextField.delegate = self
        categoryTextField.delegate = self
        
        contentView.addSubview(titleTextField)
        contentView.addSubview(categoryTextField)
        NSLayoutConstraint.activate([
            titleTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
            titleTextField.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
            titleTextField.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
            titleTextField.heightAnchor.constraint(equalToConstant: view.frame.size.height / 18),
            categoryTextField.topAnchor.constraint(equalTo: titleTextField.bottomAnchor, constant: 16),
            categoryTextField.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
            categoryTextField.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
            categoryTextField.heightAnchor.constraint(equalToConstant: view.frame.size.height / 18)
        ])
    }
    
    func setupSwitch() {
        stackView_0 = UIStackView(arrangedSubviews: [isOngoing_label, isOngoing_switch])
        stackView_0.translatesAutoresizingMaskIntoConstraints = false
        stackView_0.axis = .horizontal
        stackView_0.alignment = .center
        contentView.addSubview(stackView_0)
        
        NSLayoutConstraint.activate([
            stackView_0.topAnchor.constraint(equalTo: categoryTextField.bottomAnchor, constant: 32),
            stackView_0.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            stackView_0.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            stackView_0.heightAnchor.constraint(equalToConstant: 48)
        ])
        
    }
    
    func setupTasks() {
        contentView.addSubview(tasksLabel)
        addTasksTableViewHeightConstraint = NSLayoutConstraint(item: tableView_1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: cellSize1 * CGFloat(idea.tasks.count + 1))
        
        tableView_1.dataSource = self
        tableView_1.delegate = self
        tableView_1.rowHeight = cellSize1
        tableView_1.register(TasksCreationTableViewCell.self, forCellReuseIdentifier: "TasksCell")
        tableView_1.register(AddTaskTableViewCell.self, forCellReuseIdentifier: "AddTaskCell")
        
        view.addSubview(tableView_1)
        NSLayoutConstraint.activate([
            tasksLabel.topAnchor.constraint(equalTo: stackView_0.bottomAnchor, constant: 32),
            tasksLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            tableView_1.topAnchor.constraint(equalTo: tasksLabel.bottomAnchor, constant: 16),
            tableView_1.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            tableView_1.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            addTasksTableViewHeightConstraint!,
        ])
        
        tableView_1.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -64).isActive = true
        tableView_1.separatorColor = UIColor.clear
        tableView_1.backgroundColor = .clear
        
    }
    
    @IBAction func isOngoingSwitchStateChanged(_ sender: UISwitch) {
        idea.isOngoing = sender.isOn
    }
    
    
}

extension IdeaDetailViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return idea.tasks.count
        } else {
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TasksCell", for: indexPath) as! TasksCreationTableViewCell
            cell.taskTextField.text = idea.tasks[indexPath.row].title
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddTaskCell", for: indexPath) as! AddTaskTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == IndexPath(row: 0, section: 1) {
            idea.tasks.append(Task(title: "task1"))
            
            tableView_1.removeConstraint(addTasksTableViewHeightConstraint!)
            
            addTasksTableViewHeightConstraint = NSLayoutConstraint(item: tableView_1, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: cellSize1 * CGFloat(idea.tasks.count + 1))
            addTasksTableViewHeightConstraint!.isActive = true
            
            tableView_1.addConstraint(addTasksTableViewHeightConstraint!)
            
            tableView_1.reloadData()
            tableView_1.updateConstraints()
            tableView_1.layoutIfNeeded()
            scrollView.updateConstraints()
            scrollView.layoutIfNeeded()
        } else if indexPath.section == 0 {
            guard let cell = tableView.cellForRow(at: indexPath) as? TasksCreationTableViewCell else {
                print("Table View | didSelectRowAt: Failed to select row on tasks table view")
                return
            }
            textFieldShouldBeginEditing(cell.taskTextField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case titleTextField:
            idea.title = textField.text!
        case categoryTextField:
            categoryTextField.text = textField.text!
        default:
            print("TextFieldDidEndEditing: Failed to get text field")
            return
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        print(textField.text)
        return true
    }
    
}

class AddTaskTableViewCell: UITableViewCell {
    
    var viewBg: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1).cgColor
        return view
    }()
    
    var taskLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "+ Add Task", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(viewBg)
        viewBg.addSubview(taskLabel)
        
        NSLayoutConstraint.activate([
            viewBg.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            viewBg.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            viewBg.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            viewBg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4),
            taskLabel.centerXAnchor.constraint(equalTo: viewBg.centerXAnchor),
            taskLabel.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


class TasksCreationTableViewCell: UITableViewCell {
    
    var viewBg: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1).cgColor
        return view
    }()
    
    var isSelectedImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle")
        return imageView
    }()
    
    var taskTextField: UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
        textField.leftViewMode = .always
        var attributedPlaceholder = NSAttributedString(string: "New Task", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12), NSAttributedString.Key.foregroundColor : UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)])
        textField.font = UIFont(name: SFFont.medium.title, size: 12)
        textField.attributedPlaceholder = attributedPlaceholder
        //textField.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        textField.backgroundColor = .clear
        textField.layer.cornerRadius = 6
        return textField
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(viewBg)
        viewBg.addSubview(isSelectedImageView)
        viewBg.addSubview(taskTextField)
        
        NSLayoutConstraint.activate([
            viewBg.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            viewBg.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            viewBg.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            viewBg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4),
            isSelectedImageView.leftAnchor.constraint(equalTo: viewBg.leftAnchor, constant: 8),
            isSelectedImageView.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor),
            isSelectedImageView.heightAnchor.constraint(equalToConstant: 24),
            isSelectedImageView.widthAnchor.constraint(equalToConstant: 24),
            taskTextField.leftAnchor.constraint(equalTo: isSelectedImageView.rightAnchor, constant: 8),
            taskTextField.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
