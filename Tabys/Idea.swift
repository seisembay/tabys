//
//  Idea.swift
//  Tabys
//
//  Created by Islam Seisembay on 29/05/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

struct Idea: Equatable, Codable {
    
    var id: String
    var title: String
    var category: Category
    var tasks: [Task] = []
    
    var isOngoing = false
    
    init(uuid: String, title: String, category: Category) {
        self.id = uuid
        self.title = title
        self.category = category
    }
    
    init(uuid: String, title: String, category: Category, tasks: [Task]) {
        self.id = uuid
        self.title = title
        self.category = category
        self.tasks = tasks
    }
    
    init(uuid: String, title: String, category: Category, tasks: [Task], isOngoing: Bool) {
        self.id = uuid
        self.title = title
        self.category = category
        self.tasks = tasks
        self.isOngoing = isOngoing
    }

    
    static func == (lhs: Idea, rhs: Idea) -> Bool {
        if lhs.id == rhs.id {
            return true
        } else {
            return false
        }
    }
    
}

struct Category: Equatable, Codable {
    
    private enum CodingKeys: String, CodingKey { case oldTitle, title, colour}
    
    var oldTitle: String = ""
    
    var title: String {
        willSet {
            oldTitle = title
            //title = newValue
        }
    }
    var colour: UIColor
    
    init(title: String, colour: UIColor) {
        self.title = title
        self.colour = colour
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        oldTitle = try container.decode(String.self, forKey: .oldTitle)
        title = try container.decode(String.self, forKey: .title)
        colour = try container.decode(Color.self, forKey: .colour).uiColor
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = try encoder.container(keyedBy: CodingKeys.self)
        try container.encode(oldTitle, forKey: .oldTitle)
        try container.encode(title, forKey: .title)
        try container.encode(Color(uiColor: colour), forKey: .colour)
    }
    
    static func == (lhs: Category, rhs: Category) -> Bool {
        if lhs.title == rhs.title {
            return true
        } else {
            return false
        }
    }
    
}

struct Task: Codable {
    
    var title: String
    var isDone: Bool = false
    
}

struct IdeaController {
    
    var ideas: [Idea] = []
    
    subscript(index: Int) -> Idea {
        get {
            return ideas[index]
        }
        set {
            ideas[index] = newValue
        }
    }
    
    var categories: [Category] {
        get {
            var a = [Category(title: "All", colour: .black)]
            for i in ideas {
                if !a.contains(i.category) {
                    a.append(i.category)
                }
            }
            return a
        }
    }
    
    var ongoingIdeas: [Idea] {
        var a = [Idea]()
        for i in ideas where i.isOngoing == true {
            a.append(i)
        }
        return a
    }
    
    mutating func getData() -> [Idea] {
        if load() != nil {
            return load()!
        } else {
            return [
                    Idea(uuid: "1234", title: "Idea Create that", category: Category(title: "application", colour: UIColor.blue), tasks: [Task(title: "Do that", isDone: false), Task(title: "Get that", isDone: false)], isOngoing: true),
                    Idea(uuid: "123455", title: "Idea Create this", category: Category(title: "coding", colour: UIColor.blue), tasks: [Task(title: "code this", isDone: false), Task(title: "code that", isDone: false)]),
                    Idea(uuid: "45636", title: "Build Idea this", category: Category(title: "electronics", colour: UIColor.blue), tasks: [Task(title: "build this", isDone: false), Task(title: "build that", isDone: false)]),
                    Idea(uuid: "835636", title: "Build Idea sthis", category: Category(title: "electronics", colour: UIColor.blue), tasks: [Task(title: "build this", isDone: false), Task(title: "build that", isDone: false)], isOngoing: true),
                    Idea(uuid: "87654362", title: "Build thisIdea ", category: Category(title: "electronics", colour: UIColor.blue), tasks: [Task(title: "build this", isDone: false), Task(title: "build that", isDone: false)])
            ]
        }
    }
    
    func sortedBy(category: Category) -> [Idea] {
        if category != Category(title: "All", colour: .black) {
            var list = [Idea]()
            for i in ideas where i.category == category {
                list.append(i)
            }
            return list
        } else {
            return ideas
        }
        
    }
    
    mutating func updateIdeas(idea: Idea, isNew: Bool) {
        if isNew {
            ideas.append(idea)
        } else {
            guard let a = ideas.firstIndex(of: idea) else {
                print("Update Idea: No index for given idea")
                return
            }
            ideas[a] = idea
        }
    }
    
    func save() {
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let filePath = URL(fileURLWithPath: "Ideas", relativeTo: documentDirectory).appendingPathExtension("txt")
        
        let propertyListEncoder = PropertyListEncoder()
        let codedIdeas = try? propertyListEncoder.encode(ideas)
        try? codedIdeas?.write(to: filePath, options: .noFileProtection)
    }
    
    func load() -> [Idea]? {
        
        let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let filePath = URL(fileURLWithPath: "Ideas", relativeTo: documentDirectory).appendingPathExtension("txt")
        
        guard let codedIdeas = try? Data(contentsOf: filePath) else {return nil}
        let propertyDecoder = PropertyListDecoder()
        return try? propertyDecoder.decode(Array<Idea>.self, from: codedIdeas)
        
    }
    
}

