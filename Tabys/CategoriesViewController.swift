//
//  CategoriesViewController.swift
//  Tabys
//
//  Created by Islam Seisembay on 07/06/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
    
    var data: IdeaController!
    var sortedIdeas = [Idea]()
    var sorted = false
    var category: Category?
    
    var vcTitleLabel: UILabel = {
        var label = UILabel()
        label.text = "Categories"
        return label
    }()
    
    var searchTextField: UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
        textField.leftViewMode = .always
        var attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12), NSAttributedString.Key.foregroundColor : UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)])
        textField.font = UIFont(name: SFFont.medium.title, size: 12)
        textField.attributedPlaceholder = attributedPlaceholder
        textField.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        textField.layer.cornerRadius = 6
        return textField
    }()
    
    var categoryCollectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.estimatedItemSize.width = 100
        var collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 300, height: 300), collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    var ideasTableView: UITableView = {
        var tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.separatorColor = .clear
        return tableView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("appear")
        data.ideas = data.getData()
        sortedIdeas = data.sortedBy(category: Category(title: "All", colour: .black))
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("ddiload")
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)

        setupTextField()
        setupCollectionView()
        setupTableView()
        
    }
    
    func setupTextField() {
        searchTextField.delegate = self
        view.addSubview(searchTextField)
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 32),
            searchTextField.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),
            searchTextField.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32),
            searchTextField.heightAnchor.constraint(equalToConstant: view.frame.size.height / 18)
        ])
    }
    
    func setupCollectionView() {
        
        categoryCollectionView.register(CategorySelectCollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        
        
        view.addSubview(categoryCollectionView)
        NSLayoutConstraint.activate([
            categoryCollectionView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 16),
            categoryCollectionView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            categoryCollectionView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            categoryCollectionView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 18)
        ])
    }
    
    func setupTableView() {
        ideasTableView.delegate = self
        ideasTableView.dataSource = self
        ideasTableView.register(cvc_IdeasTableViewCell.self, forCellReuseIdentifier: "idea")
        
        let cellSize = view.frame.height / 5
        ideasTableView.rowHeight = cellSize
        
        view.addSubview(ideasTableView)
        NSLayoutConstraint.activate([
            ideasTableView.topAnchor.constraint(equalTo: categoryCollectionView.bottomAnchor, constant: 32),
            ideasTableView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),
            ideasTableView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32),
            ideasTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
        ])
        
    }

}

extension CategoriesViewController: IdeasLibrary {
    
    func getData(lib: IdeaController) {
        data = lib
    }
    
}

extension CategoriesViewController: UITextFieldDelegate {
    
}

extension CategoriesViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategorySelectCollectionViewCell
        cell.backgroundColor = .clear
        cell.categoryTitle.text = data.categories[indexPath.row].title.uppercased()
        cell.frame.size.height = view.frame.height / 19
        cell.frame.size.width = cell.categoryTitle.intrinsicContentSize.width + 16
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CategorySelectCollectionViewCell else {
            print("size for item: failed to get cell")
            return CGSize(width: 50, height: view.frame.height / 19)
        }
        return CGSize(width: cell.categoryTitle.intrinsicContentSize.width + 16, height: view.frame.height / 19)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
            sortedIdeas = data.sortedBy(category: data.categories[indexPath.row])
            print(data.categories[indexPath.row].title)
            print(sortedIdeas)
            print("did select item: selected")
        
        
        ideasTableView.reloadData()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 32)
    }
    
    
}

extension CategoriesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedIdeas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "idea", for: indexPath) as! cvc_IdeasTableViewCell
        cell.ideaLabel.text = sortedIdeas[indexPath.row].title
        cell.infoLabel.text = "\(sortedIdeas[indexPath.row].isOngoing ? "Ongoing" : "Inactive")" + " • " + "Tasks "+"\(sortedIdeas[indexPath.row].tasks.count)"
        cell.categoryLabel.text = sortedIdeas[indexPath.row].category.title.camelcased()
        return cell
    }
    
    
}

class CategorySelectCollectionViewCell: UICollectionViewCell {
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? UIColor.gray : UIColor.clear
            categoryTitle.textColor = isSelected ? UIColor.white : UIColor.gray
        }
    }
    
    var categoryTitle: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "label"
        label.textColor = UIColor.gray
        label.font = UIFont(name: SFFont.medium.title, size: 12)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        backgroundColor = .clear
        
        layer.borderColor = UIColor.gray.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = frame.height / 3
        
        addSubview(categoryTitle)
        NSLayoutConstraint.activate([
            categoryTitle.centerXAnchor.constraint(equalTo: centerXAnchor),
            categoryTitle.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class cvc_IdeasTableViewCell: UITableViewCell {
    
    var isOngoing: Bool = false {
        didSet {
            
        }
    }
    
    var bgView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.gray.cgColor
        return view
    }()
    
    var ideaLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "Project srojectProject", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.bold.title, size: 16)])
        label.attributedText = attributedString
        label.numberOfLines = 0
        return label
    }()
    
    var infoLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "isongoing i td", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        label.numberOfLines = 1
        return label
    }()
    
    var categoryLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "kotek", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        label.numberOfLines = 1
        return label
    }()
    
    var lastOpenedLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "Last Created: 12 Jule 2020", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 0.7), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        label.numberOfLines = 1
        return label

    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        bgView.layer.cornerRadius = frame.height / 3
        
        addSubview(bgView)
        NSLayoutConstraint.activate([
            bgView.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            bgView.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            bgView.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            bgView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
        
        bgView.addSubview(ideaLabel)
        bgView.addSubview(infoLabel)
        bgView.addSubview(categoryLabel)
        bgView.addSubview(lastOpenedLabel)
        NSLayoutConstraint.activate([
            ideaLabel.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 16),
            ideaLabel.leftAnchor.constraint(equalTo: bgView.leftAnchor, constant: 16),
            ideaLabel.rightAnchor.constraint(equalTo: bgView.rightAnchor, constant: -16),
            
            infoLabel.topAnchor.constraint(equalTo: ideaLabel.bottomAnchor, constant: 4),
            infoLabel.leftAnchor.constraint(equalTo: bgView.leftAnchor, constant: 16),
            infoLabel.rightAnchor.constraint(equalTo: bgView.rightAnchor, constant: -16),
            
            categoryLabel.topAnchor.constraint(equalTo: infoLabel.bottomAnchor, constant: 4),
            categoryLabel.leftAnchor.constraint(equalTo: bgView.leftAnchor, constant: 16),
            categoryLabel.rightAnchor.constraint(equalTo: bgView.rightAnchor, constant: -16),
            
            lastOpenedLabel.leftAnchor.constraint(equalTo: bgView.leftAnchor, constant: 16),
            lastOpenedLabel.rightAnchor.constraint(equalTo: bgView.rightAnchor, constant: -16),
            lastOpenedLabel.bottomAnchor.constraint(equalTo: bgView.bottomAnchor, constant: -16),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
