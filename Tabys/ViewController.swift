//
//  ViewController.swift
//  Tabys
//
//  Created by Islam Seisembay on 28/05/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import UIKit

protocol IdeasLibrary {
    func getData(lib: IdeaController)
}

class ViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITabBarDelegate {
    
    static let ideaNotification = Notification.Name("IdeaNotification")
    
    var data: IdeaController!
    var randomIdea: Idea?
    
    var delegate: IdeasLibrary?
    
    var scrollView: UIScrollView = {
        var scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        //scroll.bounces = false
        return scroll
    }()
    
    var contentView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        return view
    }()
    
    var searchTextField: UITextField = {
        var textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 16, height: 1))
        textField.leftViewMode = .always
        var attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12), NSAttributedString.Key.foregroundColor : UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)])
        textField.font = UIFont(name: SFFont.medium.title, size: 12)
        textField.attributedPlaceholder = attributedPlaceholder
        textField.backgroundColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        textField.layer.cornerRadius = 6
        return textField
    }()
    
    var randomIdeaBGView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 42/255, green: 42/255, blue: 42/255, alpha: 1)
        return view
    }()
    
    var helperLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Swipe to randomize idea"
        label.font = UIFont(name: SFFont.medium.title, size: 10)
        label.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
        return label
    }()
    
    var randomIdea_IdeaLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Some good shiz idea that noone ever thought of ever thought of ever thought of ever thought of"
        label.adjustsFontSizeToFitWidth = true
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 3
        label.font = UIFont(name: SFFont.bold.title, size: 24)
        return label
    }()
    
    var randomIdea_bgCategory: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .red
        view.layer.cornerRadius = 6
        return view
    }()
    
    var randomIdea_categoryLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "category".uppercased(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont(name: SFFont.bold.title, size: 8)])
        label.attributedText = attributedString
        return label
    }()
    
    var buttonBgView: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.backgroundColor = .white
        return view
    }()
    
    var buttonBgView_2: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.backgroundColor = .white
        return view
    }()
    
    var randomIdea_addToOngoing_button: UIButton = {
        var button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        var attributedTitle = NSAttributedString(string: "+ Add to ongoing", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 70/255, green: 70/255, blue: 70/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 14)])
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(addToOngoingButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    var randomIdea_details_button: UIButton = {
        var button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        var attributedTitle = NSAttributedString(string: "Details", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 70/255, green: 70/255, blue: 70/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 14)])
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.layer.cornerRadius = 6
        button.addTarget(self, action: #selector(detailsBtnTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    var stackView_randomIdea_buttons: UIStackView = {
        var stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .red
        return stackView
    }()
    
    var stackView_0_01: UIStackView = {
        var stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .lastBaseline
        return stackView
    }()
    
    var taskTitle: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Tasks for ongoing projects"
        label.font = UIFont(name: SFFont.medium.title, size: 12)
        label.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
        return label
    }()
    
    var viewAll_button: UIButton = {
        var button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        var attributedTitle = NSAttributedString(string: "View All", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 104/255, green: 107/255, blue: 235/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        button.setAttributedTitle(attributedTitle, for: .normal)
        button.addTarget(self, action: #selector(steset(_:)), for: .touchUpInside)
        return button
    }()
    
    var tableView: UITableView = {
        var tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layer.cornerRadius = 8
        
        tableView.alwaysBounceVertical = false
        return tableView
    }()
    
    var ongoingProjectsTitle: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Ongoing projects"
        label.font = UIFont(name: SFFont.medium.title, size: 12)
        label.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
        return label
    }()
    
    var collectionView: UICollectionView = {
        var layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        var collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: 300, height: 300), collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        return collectionView
    }()
    
    var categoriesTitle: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Categories"
        label.font = UIFont(name: SFFont.medium.title, size: 12)
        label.textColor = UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1)
        return label
    }()
    
    var tableView_1: UITableView = {
        var tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.layer.cornerRadius = 8
        tableView.alwaysBounceVertical = false
        return tableView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupIdeaData()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onNotification(notification:)), name: ViewController.ideaNotification, object: nil)
        
        setupMain()
        setupSearchTextField()
        setupRandomIdeaViews()
        setupStackView_0_01()
        setupTableView()
        setupCollectionView()
        setupTableView_1()
        
        randomizeIdea()
    }
    
    
    func setupIdeaData() {
        data.ideas = data.getData()
    }
    
    @objc func onNotification(notification: Notification) {
        print("notification received")
        
        let receivedIdea = notification.userInfo!["Idea"] as! Idea
        let isNew = notification.userInfo!["isNew"] as! Bool
        
        data.updateIdeas(idea: receivedIdea, isNew: isNew)
        
        updateCollectionViewAndTableView()
        updateRandomIdea(updatedRandomIdea: receivedIdea)
    }
    
    func setupMain() {
        
        view.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        //view.translatesAutoresizingMaskIntoConstraints = false
        
        scrollView.delegate = self
        searchTextField.delegate = self
        view.addSubview(scrollView)
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
            scrollView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0),
            scrollView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0)
        ])
        scrollView.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        scrollView.addSubview(contentView)
        NSLayoutConstraint.activate([
            contentView.leftAnchor.constraint(equalTo: scrollView.leftAnchor, constant: 0.0),
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0.0),
            contentView.rightAnchor.constraint(equalTo: scrollView.rightAnchor, constant: 0.0),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0.0),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        var heightContraint = contentView.heightAnchor.constraint(equalTo: scrollView.heightAnchor)
        heightContraint.isActive = true
        heightContraint.priority = .defaultLow
        
    }
    
    func setupSearchTextField() {
        contentView.addSubview(searchTextField)
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
            searchTextField.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            searchTextField.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            searchTextField.heightAnchor.constraint(equalToConstant: view.frame.size.height / 18)
        ])
    }
    
    func setupRandomIdeaViews() {
        
        stackView_randomIdea_buttons = UIStackView(arrangedSubviews: [buttonBgView, buttonBgView_2])
        stackView_randomIdea_buttons.frame = CGRect(x: 0, y: 0, width: 400, height: 200)
        stackView_randomIdea_buttons.translatesAutoresizingMaskIntoConstraints = false
        stackView_randomIdea_buttons.distribution = .fillEqually
        stackView_randomIdea_buttons.spacing = 16
        stackView_randomIdea_buttons.alignment = .fill
        
        contentView.addSubview(randomIdeaBGView)
        randomIdeaBGView.addSubview(helperLabel)
        randomIdeaBGView.addSubview(randomIdea_IdeaLabel)
        randomIdeaBGView.addSubview(randomIdea_bgCategory)
        randomIdea_bgCategory.addSubview(randomIdea_categoryLabel)
        
        NSLayoutConstraint.activate([
            randomIdeaBGView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 16),
            randomIdeaBGView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0),
            randomIdeaBGView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0),
            randomIdeaBGView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 3),
            
            helperLabel.topAnchor.constraint(equalTo: randomIdeaBGView.topAnchor, constant: 16),
            helperLabel.leftAnchor.constraint(equalTo: randomIdeaBGView.leftAnchor, constant: 32),
            helperLabel.heightAnchor.constraint(equalToConstant: helperLabel.intrinsicContentSize.height),
            
            randomIdea_bgCategory.topAnchor.constraint(equalTo: helperLabel.bottomAnchor, constant: 16),
            randomIdea_bgCategory.centerXAnchor.constraint(equalTo: randomIdeaBGView.centerXAnchor),
            randomIdea_bgCategory.widthAnchor.constraint(equalToConstant: randomIdea_categoryLabel.intrinsicContentSize.width + 8),
            randomIdea_bgCategory.heightAnchor.constraint(equalToConstant: randomIdea_categoryLabel.intrinsicContentSize.height + 8),
            
            randomIdea_categoryLabel.centerXAnchor.constraint(equalTo: randomIdea_bgCategory.centerXAnchor),
            randomIdea_categoryLabel.heightAnchor.constraint(equalToConstant: randomIdea_categoryLabel.intrinsicContentSize.height),
            randomIdea_categoryLabel.centerYAnchor.constraint(equalTo: randomIdea_bgCategory.centerYAnchor),
            
            randomIdea_IdeaLabel.topAnchor.constraint(equalTo: randomIdea_bgCategory.bottomAnchor, constant: 8),
            randomIdea_IdeaLabel.leftAnchor.constraint(equalTo: randomIdeaBGView.leftAnchor, constant: 32),
            randomIdea_IdeaLabel.rightAnchor.constraint(equalTo: randomIdeaBGView.rightAnchor, constant: -32),
            
        ])
        
        let recognizerView = UIView()
        recognizerView.translatesAutoresizingMaskIntoConstraints = false
        randomIdeaBGView.addSubview(recognizerView)
        recognizerView.addSubview(stackView_randomIdea_buttons)
        buttonBgView.addSubview(randomIdea_addToOngoing_button)
        buttonBgView_2.addSubview(randomIdea_details_button)
        
        NSLayoutConstraint.activate([
            recognizerView.topAnchor.constraint(equalTo: searchTextField.bottomAnchor, constant: 16),
            recognizerView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0),
            recognizerView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0),
            recognizerView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 3),
            
            stackView_randomIdea_buttons.leftAnchor.constraint(equalTo: recognizerView.leftAnchor, constant: 32),
            stackView_randomIdea_buttons.rightAnchor.constraint(equalTo: recognizerView.rightAnchor, constant: -32),
            stackView_randomIdea_buttons.bottomAnchor.constraint(equalTo: recognizerView.bottomAnchor, constant: -16),
            
            randomIdea_addToOngoing_button.topAnchor.constraint(equalTo: buttonBgView.topAnchor),
            randomIdea_addToOngoing_button.leftAnchor.constraint(equalTo: buttonBgView.leftAnchor),
            randomIdea_addToOngoing_button.rightAnchor.constraint(equalTo: buttonBgView.rightAnchor),
            randomIdea_addToOngoing_button.bottomAnchor.constraint(equalTo: buttonBgView.bottomAnchor),
            
            randomIdea_details_button.topAnchor.constraint(equalTo: buttonBgView_2.topAnchor),
            randomIdea_details_button.leftAnchor.constraint(equalTo: buttonBgView_2.leftAnchor),
            randomIdea_details_button.rightAnchor.constraint(equalTo: buttonBgView_2.rightAnchor),
            randomIdea_details_button.bottomAnchor.constraint(equalTo: buttonBgView_2.bottomAnchor),
            
            buttonBgView.heightAnchor.constraint(equalToConstant: 32)
        ])
        
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeHandler(_:)))
        swipeRecognizer.direction = [.left, .right]
        recognizerView.addGestureRecognizer(swipeRecognizer)
        
    }
    
    func setupStackView_0_01() {
        contentView.addSubview(stackView_0_01)
        stackView_0_01.addArrangedSubview(taskTitle)
        stackView_0_01.addArrangedSubview(viewAll_button)
        NSLayoutConstraint.activate([
            stackView_0_01.topAnchor.constraint(equalTo: randomIdeaBGView.bottomAnchor, constant: 32),
            stackView_0_01.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            stackView_0_01.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            stackView_0_01.heightAnchor.constraint(equalToConstant: 18)
        ])
    }
    
    func setupTableView() {
        var cellSize = view.frame.size.height / 14
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = cellSize
        tableView.register(TasksOverviewTableViewCell.self, forCellReuseIdentifier: "TaskCell")
        
        self.contentView.addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: stackView_0_01.bottomAnchor, constant: 16),
            tableView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            tableView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            tableView.heightAnchor.constraint(equalToConstant: cellSize * 3)
        ])
        
        tableView.separatorColor = UIColor.clear
        tableView.backgroundColor = .clear
        
    }
    
    func setupCollectionView() {
        contentView.addSubview(ongoingProjectsTitle)
        contentView.addSubview(collectionView)
        
        collectionView.register(OngoingProjectsCollectionViewCell.self, forCellWithReuseIdentifier: "OngoingCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        NSLayoutConstraint.activate([
            ongoingProjectsTitle.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 32),
            ongoingProjectsTitle.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            collectionView.topAnchor.constraint(equalTo: ongoingProjectsTitle.bottomAnchor, constant: 16),
            collectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 0),
            collectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: 0),
            collectionView.heightAnchor.constraint(equalToConstant: view.frame.size.height / 5)
        ])
    }
    
    func setupTableView_1() {
        self.contentView.addSubview(categoriesTitle)
        
        var cellSize1 = view.frame.size.height / 12
        tableView_1.dataSource = self
        tableView_1.delegate = self
        
        tableView_1.rowHeight = cellSize1
        tableView_1.register(CategoriesMainScreenTableViewCell.self, forCellReuseIdentifier: "CategoryCell")
        
        self.contentView.addSubview(tableView_1)
        NSLayoutConstraint.activate([
            categoriesTitle.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 32),
            categoriesTitle.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            tableView_1.topAnchor.constraint(equalTo: categoriesTitle.bottomAnchor, constant: 16),
            tableView_1.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 32),
            tableView_1.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -32),
            tableView_1.heightAnchor.constraint(equalToConstant: cellSize1 * 3)
        ])
        
        tableView_1.separatorColor = UIColor.clear
        tableView_1.backgroundColor = .clear
        tableView_1.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -64).isActive = true
        
    }
    
    func updateCollectionViewAndTableView() {
        tableView.reloadData()
        tableView_1.reloadData()
        collectionView.reloadData()
    }
    
    @IBAction func swipeHandler(_ gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.state == .ended {
            print("swipe")
            randomizeIdea()
        }
    }
    
    @IBAction func addToOngoingButtonTapped(_ sender: UIButton) {
        guard let idea = randomIdea else {
            print("Add to ongoing button: Failed to get random Idea")
            return
        }
        print(idea)
        
        guard let index = data.ideas.firstIndex(of: idea) else {
            print("Add to ongoing button: Failed to find similar Idea")
            return
        }
        
        data.ideas[index].isOngoing = true
        randomIdea?.isOngoing = true
        updateCollectionViewAndTableView()
        
        data.save()
        print(idea)
    }
    
    @objc func randomizeIdea() {
        randomIdea = data.ideas[Int.random(in: 0..<data.ideas.count)]
        randomIdea_IdeaLabel.text = randomIdea!.title
        randomIdea_categoryLabel.text = randomIdea!.category.title.uppercased()
        randomIdea_bgCategory.layoutIfNeeded()
        randomIdeaBGView.layoutSubviews()
    }
    
    func updateRandomIdea(updatedRandomIdea: Idea) {
        randomIdea = updatedRandomIdea
        randomIdea_IdeaLabel.text = randomIdea!.title
        randomIdea_categoryLabel.text = randomIdea!.category.title.uppercased()
        randomIdea_bgCategory.layoutIfNeeded()
        randomIdeaBGView.layoutSubviews()
    }
    
    @objc func steset(_ sender: UIButton) {
        print("tap")
        let a = IdeaDetailViewController()
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    @IBAction func detailsBtnTapped(_ sender: UIButton) {
        print("detailsTapped")
        guard let idea = randomIdea else {
            print("Details Button tapped: Failed to get random idea")
            return
        }
        let a = IdeaDetailViewController()
        a.idea = randomIdea
        self.navigationController?.pushViewController(a, animated: true)
    }
    
    @objc func steset2(_ sender: UIButton) {
        print("tap")
        let a = IdeaDetailViewController()
        self.navigationController?.pushViewController(a, animated: true)
    }
    
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == self.tableView {
            return 3
        } else if tableView == self.tableView_1 {
            return 4
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == self.tableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TasksOverviewTableViewCell
            cell.taskLabel.text = data.ideas[indexPath.row].tasks[0].title != "" ? data.ideas[indexPath.row].tasks[0].title : "empty"
            return cell
        } else if tableView == self.tableView_1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as! CategoriesMainScreenTableViewCell
            return cell
        }
        return UITableViewCell()
    }
    
}

extension ViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.ongoingIdeas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OngoingCell", for: indexPath) as! OngoingProjectsCollectionViewCell
        cell.categoryLabel.text = data.ongoingIdeas[indexPath.row].category.title.uppercased()
        cell.projectLabel.text = data.ongoingIdeas[indexPath.row].title
        cell.awakeFromNib()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width * 0.6, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 32, bottom: 0, right: 32)
    }
    
}

class TasksOverviewTableViewCell: UITableViewCell {
    
    var viewBg: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1).cgColor
        return view
    }()
    
    var isSelectedImageView: UIImageView = {
        var imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle")
        return imageView
    }()
    
    var taskLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "Test table", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.light.title, size: 12)])
        label.attributedText = attributedString
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        addSubview(viewBg)
        viewBg.addSubview(isSelectedImageView)
        viewBg.addSubview(taskLabel)
        
        NSLayoutConstraint.activate([
            viewBg.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            viewBg.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            viewBg.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            viewBg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4),
            isSelectedImageView.leftAnchor.constraint(equalTo: viewBg.leftAnchor, constant: 8),
            isSelectedImageView.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor),
            isSelectedImageView.heightAnchor.constraint(equalToConstant: 24),
            isSelectedImageView.widthAnchor.constraint(equalToConstant: 24),
            taskLabel.leftAnchor.constraint(equalTo: isSelectedImageView.rightAnchor, constant: 8),
            taskLabel.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class CategoriesMainScreenTableViewCell: UITableViewCell {
    
    var viewBg: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1).cgColor
        return view
    }()
    
    var categoryLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "category".uppercased(), attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        return label
    }()
    
    var numberOfItemsLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "24\nideas", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 105/255, green: 105/255, blue: 105/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.medium.title, size: 12)])
        label.attributedText = attributedString
        label.numberOfLines = 2
        label.textAlignment = .right
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        backgroundColor = .clear
        
        addSubview(viewBg)
        viewBg.addSubview(categoryLabel)
        viewBg.addSubview(numberOfItemsLabel)
        
        NSLayoutConstraint.activate([
            viewBg.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            viewBg.leftAnchor.constraint(equalTo: leftAnchor, constant: 0),
            viewBg.rightAnchor.constraint(equalTo: rightAnchor, constant: 0),
            viewBg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4),
            categoryLabel.leftAnchor.constraint(equalTo: viewBg.leftAnchor, constant: 16),
            categoryLabel.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor),
            numberOfItemsLabel.rightAnchor.constraint(equalTo: viewBg.rightAnchor, constant: -16),
            numberOfItemsLabel.centerYAnchor.constraint(equalTo: viewBg.centerYAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class OngoingProjectsCollectionViewCell: UICollectionViewCell {
    
    var projectLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "Project srojectProject", attributes: [NSAttributedString.Key.foregroundColor : UIColor(red: 80/255, green: 80/255, blue: 80/255, alpha: 1), NSAttributedString.Key.font : UIFont(name: SFFont.bold.title, size: 14)])
        label.attributedText = attributedString
        label.numberOfLines = 0
        return label
    }()
    
    var bgCategory: UIView = {
        var view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 82/255, green: 176/255, blue: 234/255, alpha: 1)
        view.layer.cornerRadius = 6
        return view
    }()
    
    var categoryLabel: UILabel = {
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        var attributedString = NSAttributedString(string: "category".uppercased(), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont(name: SFFont.bold.title, size: 8)])
        label.attributedText = attributedString
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(projectLabel)
        addSubview(bgCategory)
        bgCategory.addSubview(categoryLabel)
        
        layer.cornerRadius = 8
        layer.borderWidth = 1
        layer.borderColor = UIColor(red: 227/255, green: 227/255, blue: 227/255, alpha: 1).cgColor
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NSLayoutConstraint.activate([
            projectLabel.topAnchor.constraint(equalTo: topAnchor, constant: 16),
            projectLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            projectLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -16),
            bgCategory.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            bgCategory.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
            bgCategory.heightAnchor.constraint(equalToConstant: frame.size.height / 8),
            bgCategory.widthAnchor.constraint(equalToConstant: categoryLabel.intrinsicContentSize.width + 8),
            categoryLabel.centerXAnchor.constraint(equalTo: bgCategory.centerXAnchor),
            categoryLabel.centerYAnchor.constraint(equalTo: bgCategory.centerYAnchor)
        ])
    }
    
}

enum SFFont {
    
    case regular, bold, light, medium, semibold
    
    var title: String {
        switch self {
        case .bold:
            return "SFProText-Bold"
        case .light:
            return "SFProText-Light"
        case .medium:
            return "SFProText-Medium"
        case .regular:
            return "SFProText-Regular"
        case .semibold:
            return "SFProText-SemiBold"
        }
    }
    
}

