//
//  Extentions.swift
//  Tabys
//
//  Created by Islam Seisembay on 05/06/2020.
//  Copyright © 2020 seisembay. All rights reserved.
//

import Foundation
import UIKit

struct Color: Codable {
    
    var red: CGFloat = 0.0
    var green: CGFloat = 0.0
    var blue: CGFloat = 0.0
    var alpha: CGFloat = 0.0
    
    var uiColor: UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    init(uiColor: UIColor) {
        uiColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
    }
    
}

extension String {
    
    func camelcased() -> String {
        var b: [Character] = []
        for i in self {
            b.append(i)
        }
        b[0] = Character(b[0].uppercased())
        var c: String = ""
        for i in b {
            c.append(String(i))
        }
        return c
    }
    
}

